Hooks.once('ready', () => {
	game.settings.register("Foundry UI - Compact", "compactUI", {
		name: "Compact UI",
		hint: "Remove unnecessary user interface elements and compact the sizes for smaller screens.",
		scope: "client",
		config: true,
		type: Boolean,
		default: true
	});
    game.settings.register("Foundry UI - Compact", "tabbedLog", {
		name: "Tabbed Log",
		hint: "Separates the chat log into three distinct tabs for OOC, IC, and Rolls.",
		scope: "client",
		config: true,
		type: Boolean,
		default: true
	});
});
